//Variables Declared
var search, autocomplete;
var markersArray = [];
var map;
var marker;

//Fields of location
var componentForm = {
    locality: 'long_name',
    administrative_area_level_1: 'short_name',
    country: 'long_name',
};

//Start of main working
function initialize() {
    var mapOptions = {
        center: new google.maps.LatLng(31, 80),
        zoom: 4
    };
    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    autocomplete = new google.maps.places.Autocomplete((document.getElementById('auto-complete')), {types: ['geocode']});
    google.maps.event.addListener(autocomplete, 'place_changed', function() {

// Getting the location details from the autocomplete object and making that point marked on map.
        var place = autocomplete.getPlace();
        for (var component in componentForm) {
            document.getElementById(component).value = '';
            document.getElementById(component).disabled = false;
        }
        var newPos = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
        map.setOptions({
            center: newPos,
            zoom: 12
        });

//adding a marker at searched location
        marker = new google.maps.Marker({
            position: newPos,
            map: map,
            title: "New marker"
        });
// pushing marker in markersArray array       
        markersArray.push(marker);
// Get each field of location from the location details and filling the corresponding form fields.
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
            }
        }
        document.getElementById("lati").value = newPos.lat();
        document.getElementById("lng").value = newPos.lng();
    });

//    functions to perform when map is clicked
    google.maps.event.addListener(map, 'click', function(event) {
        placeMarker(event.latLng);//calling placeMarker function for inserting markers on map 
        document.getElementById("auto-complete").value = "";
        lat = event.latLng.lat();
        lng = event.latLng.lng();
        var latlng = new google.maps.LatLng(lat, lng);
        document.getElementById("lati").value = lat;
        document.getElementById("lng").value = lng;
// creating geocoder object
        var geocoder = new google.maps.Geocoder();

        geocoder.geocode({'latLng': latlng}, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK)
            {
// if loaction of place searched found, pass to process function
                for (var i = 0; i < results[0].address_components.length; i++)
                {
                    var addr = results[0].address_components[i];
                    if (addr.types[0] === 'country')
                    {
                        document.getElementById("country").value = addr.long_name;
                    }
                    if (addr.types[0] === 'locality')
                    {
                        document.getElementById("locality").value = addr.long_name;
                    }
                    if (addr.types[0] === 'administrative_area_level_1')
                    {
                        document.getElementById("administrative_area_level_1").value = addr.long_name;
                    }
                }
            }
        });
    });
}
google.maps.event.addDomListener(window, 'load', initialize);

//used for adding the markers on the place clicked on map
function placeMarker(latlng) {
    var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        title: "marker"
    });
    deleteMarkers();
    markersArray.push(marker);
}

//to delete previous markers from map
function deleteMarkers(map) {

    for (var i = 0; i < markersArray.length; i++) {
        markersArray[i].setMap(null);
    }
    markersArray = [];

} 